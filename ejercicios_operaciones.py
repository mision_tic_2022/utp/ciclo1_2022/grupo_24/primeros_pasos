'''
1) Desarrolle un programa que eleve al cuadrado una variable 
(inicializada con cualquier valor).
2) El resultado del punto #1 dividalo por 2
y muestre la operación completa en consola.
Ejemplo
    (10 ^ 2) /2

NOTA: Pueden inicializar la variable con cualquier número
'''
numero = 8
cuadrado = numero ** 2
division = cuadrado / 2

mensaje = f"({numero} ^ 2) / 2 = {division}"
#print(mensaje)

'''
3) Muestre el siguiente mensaje en consola utilizando un solo print:
    ---------------------------------
    -   Soy tripulante  -
    -   Misión Tic  -
    --------------------------------
    NOTA:
        Los espaciados dentro del recuadro son tabulaciones (tab)
'''

#punto_3 = "---------------------------------\n-\tSoy tripulante\t-\n-\tMision tic\t-\n---------------------------------"
punto_3 = "---------------------------------\n"
punto_3 = punto_3 + "\tSoy tripulante\t-\n"
punto_3 = punto_3 + "\n-\tMision tic\t-\n"
punto_3 = punto_3 + "---------------------------------"
print(punto_3)

#Solución de Blanca Yanine
#print("--------------------------\n-\tSoy tripulante\t -\n-\tMisión TIC\t-\n--------------------------")