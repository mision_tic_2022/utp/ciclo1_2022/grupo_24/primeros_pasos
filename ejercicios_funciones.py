
'''
1)Desarrolle una función que reciba como parámetro el nombre del usuario y la ciudad. 
La función deberá retornar:
    -> Hola nombre_usuario nos alegra que nos visites desde nombre_ciudad
2) Desarrolle una función que calcule y retorne el promedio de 4 notas de un estudiante
3) Desarrolle una función que retorne los intereses a ganar de un CDT:
    FORMULA PARA CALCULAR LOS INTERESES: (cantidad_dinero * porcentaje_interes * tiempo) / 12
'''

#Punto 1
def saludar(usuario, ciudad):
    return "Hola "+usuario+" nos algra que nos visites desde "+ciudad

#print( saludar('Jenifer', 'Bogotá') )

def calcular_promedio(nota_1: float, nota_2: float, nota_3: float, nota_4: float):
    #Calcular promedio
    resp = (nota_1 + nota_2 + nota_3 + nota_4) / 4
    #Retornar promedio
    return resp


#--------SOLUCIÓN DE TRIPULANTES-------------
#Solución de Blanca
def datos(nom: str, ciudad: str):
    return f"hola {nom}, nos alegra que nos visites desde {ciudad}"

#Sebastián Andrés
def promedio(num1: float, num2: float, num3: float, num4: float):
    suma = num1 + num2 + num3 + num4
    resultPromedio = suma / 4
    return resultPromedio

#Julio cesar
def cdt(can_dinero: int, porcentaje_interes: float, tiempo: int):
    
    return(can_dinero * porcentaje_interes * tiempo) / 12

#print(f"El Interes a ganar del CDT es: {cdt(200000, 3, 10)}")


#Operaciones básicas

def multiplicar(n1: float, n2: float, n3: float):
    mult = n1 * n2 * n3
    return mult

resp_1 = multiplicar(50, 20, 30) 
resp_2 = cdt(resp_1, 0.4, 8)
print(f"El valor del interés es de {resp_2}")

def operacion():
    mult = multiplicar(30, 40, 50)
    mult /= 2
    return mult

print( operacion() )