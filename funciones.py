
#def -> Palabra reservada para crear funciones

#Función sin parámetros y sin retorno
def saludar():
    mensaje = "Hola mundo dentro de una funcion"
    print(mensaje)

#saludar()

'''
Estructura de una función:

def nombre_funcion( parámetros(opcionales)):
    mi código
    return (opcional)
'''

#Funciones sin parámetros y con retorno
def sumar():
    num_1 = 10
    num_2 = 20
    return num_1 + num_2

print( sumar() )
'''
resultado = sumar()
resultado += 10
print(resultado)
'''

#Funciones con parámetro
def sumar_con_parametros(num_1: float, num_2: float):
    sum = num_1 + num_2
    return sum


resp = sumar_con_parametros(10, 90)
print( resp )

def saludar_usuario(nombre: str):
    return f"Hola {nombre}, nos alegra que nos visites"

print( saludar_usuario('Ana') )

